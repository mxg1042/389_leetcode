# README #

Problem 389 Leetcode

### Contribution guidelines ###

* Two methods to solve : first is using map. second is using XOR. 
* Using map : increase the number of characters present in the string s; and then decrease the number for the characters present in string t. Extra character will eventually have value -1.
* Using XOR : as there is only one extra character, XOR everything and eventually that extra charcater will remain. 

